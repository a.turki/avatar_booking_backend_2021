package org.ab.email;

public interface EmailSender {
    void send(String to, String email);
    void send2(String to, String email);
    void sendNotif(String to, String email);
}
