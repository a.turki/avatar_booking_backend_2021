package org.ab.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import org.bson.types.Binary;

@Getter
@Setter
@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString
@Document(collection = "photos")
public class Photo {
	@Id
	private String id;
	private String name;
	private String type;
	private byte[] picByte;
	private String theuser;
	
	public Photo(String name, String type, byte[] picByte, String theuser) {
		super();
		this.name = name;
		this.type = type;
		this.picByte = picByte;
		this.theuser = theuser;
	}
	


	
	
	    
	    

	    
}
