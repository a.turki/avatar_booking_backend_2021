package org.ab.models;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Document("resetpwdtoken")
public class PasswordRecoveryToken {
	 	@Id
	    private String id;
	    private String token;
	    private LocalDateTime createdAt;
	    private LocalDateTime expiresAt;
	    private LocalDateTime confirmedAt;
	    private  String usermail;
	    
	    
	    
	    
	    
		public PasswordRecoveryToken(String token, LocalDateTime createdAt, LocalDateTime expiresAt,
				LocalDateTime confirmedAt, String usermail) {
			super();
			this.token = token;
			this.createdAt = createdAt;
			this.expiresAt = expiresAt;
			this.confirmedAt = confirmedAt;
			this.usermail = usermail;
		}





		public PasswordRecoveryToken(String token, LocalDateTime createdAt, LocalDateTime expiresAt, String usermail) {
			super();
			this.token = token;
			this.createdAt = createdAt;
			this.expiresAt = expiresAt;
			this.usermail = usermail;
		}
	    
	    
	    
	    
}
