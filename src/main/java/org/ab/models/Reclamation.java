package org.ab.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(collection = "reclamation")
public class Reclamation {
	@Id
	private String id;
	private String title;
	private String type;
	private String description;
	private boolean processed;
	private String missionID;
	//@DBRef	
	private Mission mission;
	private String decision;
	
	private String claimerName;
	private User claimer;
	
	
	
	
	

}
