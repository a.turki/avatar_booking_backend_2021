package org.ab.models;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@ToString
@Document
public class Message {
	@Id
	private String id;
	private String senderId;
	private String receiverId;
	private String content;
	private Date sentAt ;
	private boolean seen;
	
}
