package org.ab.models;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@ToString
@Document
public class Mission {
	@Id
	private String id;
	private String title;
	private String description;
	private int price;
	private String address;
	private String country;
	private String region;
	private Date date ;
	private Date expiredate ;
	private String time ;
	private int duration ;
	private String preference ;
	private String clientemail;
	private String type ;
	private List<String> listavatarCandidat;
	private List<String> listavatarDeclined;
	private List<String> listavatarInterested;
	private String avataremail;
	private String state;
	private String progress;
	private boolean rated;
	private boolean expired;
	/*@DBRef*/
	private User avatar;
	// @DBRef
	private User client;
	
	private List<Reclamation> reclamations;
	private List<Offer> offers;
	private List<String> claimed;
	
	/*public Mission(String state) {
		super();
		this.state = state;
	}*/
	public Mission(String id,
					String title,
					String description,
					int price,
					String address,
					String country,
					String region,
					Date date,
					Date expiredate,
					String time,
					int duration,
					String preference,
					String clientemail,
					String type,
					List<String> listavatarCandidat,
					List<String> listavatarDeclined,
					List<String> listavatarInterested,
					String avataremail,
					String state,
					String progress,
					boolean rated,
					boolean expired,
					User avatar,
					User client,
					List<Reclamation>reclamations,
					List<Offer>offers,
					List<String> claimed) {
		
		this.id = id;
		this.title = title;
		this.description = description;
		this.price = price;
		this.address = address;
		this.country = country;
		this.region = region;
		this.date = date;
		this.expiredate = expiredate;
		this.time = time;
		this.duration = duration;
		this.preference = preference;
		this.clientemail = clientemail;
		this.type = type;
		this.listavatarCandidat = listavatarCandidat;
		this.listavatarDeclined = listavatarDeclined;
		this.listavatarInterested = listavatarInterested;
		this.avataremail = avataremail;
		this.state = state;
		this.progress = progress;
		this.rated= rated ;
		this.expired= expired;
		this.avatar=avatar;
		this.client=client;
		this.reclamations=reclamations;
		this.offers=offers;
		this.claimed=claimed;
		
		
		
			
	}
	

	
}
