package org.ab.models;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document
public class Notification {
	@Id
	private String id;
	private String senderUsername;
	private String recipientUsername;
	private String type;
	// private String content;
	private String missionID;
	//@DBRef
	//private Mission mission;
	private boolean seen;
	private Date sentAt;
	private Date seenAt;

}
