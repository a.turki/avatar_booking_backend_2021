package org.ab.models;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document
public class Offer {
	
	@Id
	private String id;
	private String missionId;
	private String avatarMail;
	//private String avatarId;
	private User avatar;
	private int price;
	private String state;
	private boolean accepeted;
	private Date date ;
	
}
