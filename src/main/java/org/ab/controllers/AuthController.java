package org.ab.controllers;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.ab.email.EmailSender;
import org.ab.models.ERole;
import org.ab.models.PasswordRecoveryToken;
import org.ab.models.Role;
import org.ab.models.User;
import org.ab.payload.request.LoginRequest;
import org.ab.payload.request.ResetPwdRequest;
import org.ab.payload.request.SignupRequest;
import org.ab.payload.response.JwtResponse;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.PasswordRecoveryTokenRepository;
import org.ab.repository.RoleRepository;
import org.ab.repository.UserRepository;
import org.ab.security.jwt.JwtUtils;
import org.ab.services.UserDetailsImpl;
import org.ab.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;





@CrossOrigin(origins = "*")
@RestController
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	PasswordRecoveryTokenRepository passwordRecoveryTokenRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;
	@Autowired
	UserDetailsServiceImpl userDetailsServiceImpl;
	
	private final EmailSender emailSender;
	MongoTemplate mongoTemplate;


	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		
		boolean exist=userRepository.existsByUsername(loginRequest.getUsername());
		//verification
		System.out.println("exist="+exist);
		if(exist){
    		System.out.println("test exist valid");

			Query query1=new Query();
	    	query1.addCriteria(Criteria.where("username").is(loginRequest.getUsername()));
	    	User ct=mongoTemplate.findOne(query1, User.class);
	    	if(ct.isEnabled())
	    	{	
	    		System.out.println("test enabled valid");
	    		Authentication authentication = authenticationManager.authenticate(
	    				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

	    		SecurityContextHolder.getContext().setAuthentication(authentication);
	    		String jwt = jwtUtils.generateJwtToken(authentication);
	    		
	    		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
	    		List<String> roles = userDetails.getAuthorities().stream()
	    				.map(item -> item.getAuthority())
	    				.collect(Collectors.toList());
 
	    		return ResponseEntity.ok(new JwtResponse(jwt, 
	    												 userDetails.getId(), 
	    												 userDetails.getUsername(), 
	    												 userDetails.getEmail(), 
	    												 roles,
	    												 ct.isEnabled()));
	    	}
	    	else 
	    	return ResponseEntity
					.badRequest()
					.body(new MessageResponse("User not enabled!"));
		}
		else 
		return ResponseEntity
				.badRequest()
				.body(new MessageResponse("Error: User does not exist!"));
	}
	
	@PostMapping("/newpassword")
	public ResponseEntity<?> newpassword(@RequestBody ResetPwdRequest resetPwdRequest){
		//boolean exist=userRepository.existsByUsername(resetPwdRequest.getEmail());
		boolean exist=userRepository.existsByEmail(resetPwdRequest.getEmail());
		boolean exist2=passwordRecoveryTokenRepository.existsByUsermail(resetPwdRequest.getEmail());
		
		if(!exist2){
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("No Recovery Demand Found"));
			
		}
		
		if(exist){
			Query query1=new Query();
			Query query2=new Query();
	    	query1.addCriteria(Criteria.where("email").is(resetPwdRequest.getEmail()));
	    	query2.addCriteria(Criteria.where("usermail").is(resetPwdRequest.getEmail()));
	    	PasswordRecoveryToken ut=mongoTemplate.findOne(query2, PasswordRecoveryToken.class);
	    	User ct=mongoTemplate.findOne(query1, User.class);
	    	int tb=ut.getExpiresAt().compareTo(LocalDateTime.now());
	    	System.out.println("dtae "+LocalDateTime.now());
	    	System.out.println("******************************* "+tb);
	    	if(tb>0)
	    	{
	    		System.out.println("new passw " + resetPwdRequest.getEmail() +resetPwdRequest.getPassword());
	    	ct.setPassword(encoder.encode(resetPwdRequest.getPassword()));
			mongoTemplate.save(ct);
			System.out.println("password updated "+ resetPwdRequest.getPassword());
			mongoTemplate.remove(ut);
			System.out.println("token removed");
			return ResponseEntity.ok(new MessageResponse("Password Updated Token valid"));
			}
	    	else 
	    		mongoTemplate.remove(ut);
	    		return ResponseEntity.badRequest()
					.body(new MessageResponse("Token expired"));
		}
		else return ResponseEntity.badRequest()
				.body(new MessageResponse("User does not exist!"));
	}
	
	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(), 
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRoles();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_CLIENT)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
					//mod
				case "avatar":
					Role modRole = roleRepository.findByName(ERole.ROLE_AVATAR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);
					user.setIsavatar(true);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_CLIENT)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);
		String link = "http://localhost:8080/api/auth/confirm?nom="+signUpRequest.getUsername() ;
	    //String link="test" ;
	    
		emailSender.send(user.getEmail(),buildEmail(user.getUsername(), link));
	    //emailSender.send("yassinetaktak9@gmail.com",buildEmail("yassine", link));

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
	
	@GetMapping(path = "confirm")
    public ResponseEntity<?> confirm(@RequestParam("nom") String nom) {
    	//userDetailsServiceImpl.enableAppUser(mail);
    	Query query1=new Query();
    	query1.addCriteria(Criteria.where("username").is(nom));
    	
    	User ct=mongoTemplate.findOne(query1, User.class);
		System.out.println(ct.getEmail());
    	ct.setEnabled(true);
    	System.out.println("enabled");
    	mongoTemplate.save(ct);
    	System.out.println("yesssssssssssssss-------------");
    	return ResponseEntity.ok(new MessageResponse("User activated!"));
    }
	
	
	
	private String buildEmail(String name, String link)
     {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote>\n Link will expire in 15 minutes. <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }
	@GetMapping("/allAvatar")
		public List<User> getAllAvatar(){
			Optional<Role> E = this.roleRepository.findByName(ERole.ROLE_AVATAR);
			List<User>avatars=new ArrayList<User>();
			List<User> ct = this.userRepository.findAll();
			
			for (int i = 0; i < ct.size();i++)
			  {
				for(Role s : ct.get(i).getRoles())
					  if(s.getName().equals(ERole.ROLE_AVATAR))
				        {avatars.add(ct.get(i));
				        	
						  System.out.println(s.getName());}
			  }
			System.out.println("************list of avatar getted****************");

				return avatars;
		}
	 
	@SuppressWarnings("deprecation")
	@GetMapping("/user/{id}")
	//@PreAuthorize("hasRole('CLIENT') or hasRole('AVATAR') or hasRole('ADMIN')")
	public ResponseEntity<User> getUserByID(@PathVariable("id") String id) {
	  Optional<User> userData = userRepository.findById(id);
	  boolean exist=userRepository.existsByUsername(userData.get().getUsername());
	  System.out.println("exist profile="+exist);
	  if (exist) {
		  System.out.println("mawjjoud");
		  System.out.println(userData.get().getDateOfBirth());
	    return new ResponseEntity<>(userData.get(), HttpStatus.OK);
	  } else {
		  System.out.println("mech mawjjoud");
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	  }
	}
	 
	
}