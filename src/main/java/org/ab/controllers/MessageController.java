package org.ab.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.ab.models.Message;
import org.ab.models.Mission;
import org.ab.models.Notification;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.MessageRepository;
import org.ab.repository.MissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class MessageController {
	@Autowired	
	MessageRepository messageRepository;
	
	@PostMapping("/sendMessage")
	ResponseEntity<?> sendMessage(@RequestBody Message message) {
		LocalDateTime instance = LocalDateTime.now();
		message.setSentAt(Date.from(instance.atZone(ZoneId.systemDefault()).toInstant()));
		message.setSeen(false);
		
		this.messageRepository.save(message);
		System.out.println("*********************************message sent*********************************");
		return ResponseEntity.ok(new MessageResponse("Message Sent Succefully"));
	}
	
	// get user1 and user2 messages exchanged
		@GetMapping("/discussion/{username1}/{username2}")
		public List<Message> getDiscussion(@PathVariable(value = "username1") String username1,
										@PathVariable(value = "username2") String username2)
		{
		List<Message> Discussions=new ArrayList<Message>();
		List<Message> ct = this.messageRepository.findAll();
		
		
		
		for (int i=0;i< ct.size();i++)
		{
			if(((ct.get(i).getSenderId().equals(username1))&&(ct.get(i).getReceiverId().equals(username2)))||
				((ct.get(i).getSenderId().equals(username2))&&(ct.get(i).getReceiverId().equals(username1))))
			{
			
			Discussions.add(ct.get(i));
			}

				

			
		}
					System.out.println("discusion getted ");
					return Discussions;
			  }
		
		@GetMapping("/listdiscussion/{username1}")
		public List<String> getListDiscussion(@PathVariable(value = "username1") String username1)
		{
			List<String> Discussions=new ArrayList<String>();
			List<Message> ct = this.messageRepository.findAll();
			
			//List<Message> ct = this.messageRepository.getDiscussionsUser(username1);			
			for (int i=0;i< ct.size();i++)
			{
				if(ct.get(i).getReceiverId().equals(username1))
				{
				
					if(!Discussions.contains(ct.get(i).getSenderId())){
						Discussions.add(ct.get(i).getSenderId());
					}
				
				}else if (ct.get(i).getSenderId().equals(username1))
				{
				
					if(!Discussions.contains(ct.get(i).getReceiverId())){
						Discussions.add(ct.get(i).getReceiverId());
					}
				
				}

					

				
			}
			
			
			return Discussions;
		}
		
		// get user1 and user2 LAST messages exchanged
		@GetMapping("/lastmessage/{username1}/{username2}")
		public Message getLastMessage( String username1,
									   String username2)
		{
		List<Message> Discussions=new ArrayList<Message>();
		List<Message> ct = this.messageRepository.findAll();
		
		
		
		for (int i=0;i< ct.size();i++)
		{
			if(((ct.get(i).getSenderId().equals(username1))&&(ct.get(i).getReceiverId().equals(username2)))||
				((ct.get(i).getSenderId().equals(username2))&&(ct.get(i).getReceiverId().equals(username1))))
			{
			
			Discussions.add(ct.get(i));
			}

				

			
		}
					System.out.println("discusion getted ");
					System.out.println(Discussions.get(Discussions.size()-1).getContent());
					return Discussions.get(Discussions.size()-1);
			  }		
		
	
		
		@GetMapping("/getMyLastMessages/{username1}")
		public List<Message> getMyLastMessage(@PathVariable(value = "username1") String username1){
			List<Message> MylastMessages=new ArrayList<Message>();
			List<String> Discussions=new ArrayList<String>();
			Discussions=getListDiscussion(username1);
			//Discussions=getListDiscussion2(username1);
			
			for (int i=0;i< Discussions.size();i++)
			{
				MylastMessages.add(getLastMessage(username1,Discussions.get(i)));
				
			}
			
			
			
			return MylastMessages;
		}
	
	
		
		// with repo
		@GetMapping("/discussion2/{username1}/{username2}")
		public List<Message> getDiscussion2(@PathVariable(value = "username1") String username1,
										@PathVariable(value = "username2") String username2)
		{
			List<Message> Discussion = this.messageRepository.getDiscussionsU1U2(username1, username2);
			
			
			return Discussion;
		}
		@GetMapping("/discussion2unseen/{username1}/{username2}")
		public int unseenMessageCount(@PathVariable(value = "username1") String username1,
				@PathVariable(value = "username2") String username2)
		{
		
			return this.messageRepository.unseenMessagecount(username1, username2,false);
		}
		
		@PutMapping("/CheckMessage/{id}")
		public ResponseEntity<?> checkMessage(@PathVariable(value = "id") String id)  {
		Optional<Message> retrievedMessage = messageRepository.findById(id);
		retrievedMessage.get().setSeen(true);
		messageRepository.save(retrievedMessage.get());
		System.out.println("Message Checked");
		return ResponseEntity.ok(retrievedMessage);

	  }
		
		@GetMapping("/listdiscussion2/{username1}")
				public List<String> getListDiscussion2(@PathVariable String username1)
				{
					List<String> Discussions=new ArrayList<String>();
					List<Message> ct = this.messageRepository.getDiscussionsUser(username1);
					
					
					
					for (int i=0;i< ct.size();i++)
					{
						if(ct.get(i).getReceiverId().equals(username1))
						{
						
							if(!Discussions.contains(ct.get(i).getSenderId())){
								Discussions.add(ct.get(i).getSenderId());
							}
						
						}else if (ct.get(i).getSenderId().equals(username1))
						{
						
							if(!Discussions.contains(ct.get(i).getReceiverId())){
								Discussions.add(ct.get(i).getReceiverId());
							}
						
						}

							

						
					}
					
					
					return Discussions;
				}
				
				@GetMapping("/lastmessage2/{username1}/{username2}")
				public Message getLastMessage2(@PathVariable String username1,
						@PathVariable String username2)
				{		
					List<Message> Discussion= this.getDiscussion2(username1, username2);
					System.out.println(Discussion.get(Discussion.size()-1).getContent());
					
					return Discussion.get(Discussion.size()-1);
					
				}			
				
				@GetMapping("/allmessage/{username1}")
				public List<Message> getDiscussion2(@PathVariable(value = "username1") String username1)
				{
					return this.messageRepository.getDiscussionsUser(username1);
				}
				
				@GetMapping("/countunseen/{username1}/{username2}")
				public int getCountunseen(@PathVariable String username1,
						@PathVariable String username2)
				{
				
					return this.messageRepository.unseenMessagecount(username1, username2, false);
					
				}
		
		
}
