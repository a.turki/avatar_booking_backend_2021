package org.ab.controllers;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;

import org.ab.email.EmailSender;
import org.ab.models.ERole;
import org.ab.models.PasswordRecoveryToken;
import org.ab.models.Role;
import org.ab.models.User;
import org.ab.payload.request.RecoverRequest;
import org.ab.payload.request.SignupRequest;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.PasswordRecoveryTokenRepository;
import org.ab.repository.RoleRepository;
import org.ab.repository.UserRepository;
import org.ab.security.jwt.JwtUtils;
import org.ab.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
@CrossOrigin(origins = "*")
@RestController
@AllArgsConstructor
@RequestMapping("/tak/test")
public class PasswordRecoveryTokenController {

	@Autowired
	UserRepository userRepository;
	@Autowired
	PasswordRecoveryTokenRepository passwordRecoveryTokenRepository;
	@Autowired
	PasswordEncoder encoder;
	private final EmailSender emailSender;
	MongoTemplate mongoTemplate;
	
	@PostMapping("/resetpwd")
	public ResponseEntity<?> resetPwdDemand(@Valid @RequestBody RecoverRequest recoverRequest) {
				
		if (userRepository.existsByEmail(recoverRequest.getEmail())){
			
			Query query1=new Query();
	    	query1.addCriteria(Criteria.where("email").is(recoverRequest.getEmail()));
	    	User ct=mongoTemplate.findOne(query1, User.class);
			
			
			//String link = "http://localhost:8080/api/test/resettoken?email="+recoverRequest.getEmail() ;
			String link = "http://localhost:4200/recovpassword/"+recoverRequest.getEmail() ;

	    	emailSender.send2(ct.getEmail(),buildEmail(ct.getUsername(), link));
		    System.out.println("mail sent");
		    
		    this.resettoken(recoverRequest.getEmail());
		    

			return ResponseEntity.ok(new MessageResponse("Password reset demand SUCCESS !"));
		
			
		}
		else return ResponseEntity.badRequest().body(new MessageResponse("Password reset demand Failed !"));
		//return ResponseEntity.ok();
	}
	
	
	
	@GetMapping(path = "resettoken")
    public ResponseEntity<?> resettoken(@RequestParam("email") String email) {
//    	//userDetailsServiceImpl.enableAppUser(mail);
//    	Query query1=new Query();
//    	query1.addCriteria(Criteria.where("username").is(email));
//    	
//    	User ct=mongoTemplate.findOne(query1, User.class);
//		System.out.println(ct.getEmail());
//    	ct.setEnabled(true);
		//mongoTemplate.save(ct);
    	System.out.println("reset token method enter");
    	String token = UUID.randomUUID().toString();
    	PasswordRecoveryToken PRT=new PasswordRecoveryToken(token,LocalDateTime.now(),
    			LocalDateTime.now().plusMinutes(30),email);
    	passwordRecoveryTokenRepository.save(PRT);
    	
    	
    	System.out.println("token saved");
    	return ResponseEntity.ok(new MessageResponse("reset token created"));
    }
	
	
	
	 private String buildEmail(String name, String link)
     {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Reset your Password</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Please click on the below link to reset your account's password: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Reset Password</a> </p></blockquote>\n Link will expire in 30 minutes. <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }

	
	
	
}
