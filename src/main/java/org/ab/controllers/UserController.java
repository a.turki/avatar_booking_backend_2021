package org.ab.controllers;

import java.util.List;
import java.util.Optional;

import org.ab.email.EmailSender;
import org.ab.exception.ResourceNotFoundException;
import org.ab.models.Mission;
import org.ab.models.User;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.RoleRepository;
import org.ab.repository.UserRepository;
import org.ab.security.jwt.JwtUtils;
import org.ab.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import lombok.AllArgsConstructor;

@CrossOrigin(origins = "*")
@RestController
@AllArgsConstructor
@RequestMapping("/api/profile")
public class UserController {
	@Autowired
	UserRepository userRepository;
	MongoTemplate mongoTemplate;

	
	@SuppressWarnings("deprecation")
	@GetMapping("/user/{username}")
	@PreAuthorize("hasRole('CLIENT') or hasRole('AVATAR') or hasRole('ADMIN')")
	public ResponseEntity<User> getUserByUsername(@PathVariable("username") String username) {
	  Optional<User> userData = userRepository.findByUsername(username);
	  boolean exist=userRepository.existsByUsername(username);
	  System.out.println("exist profile="+exist);
	  if (exist) {
		  System.out.println("mawjjoud");
		  System.out.println(userData.get().getDateOfBirth());
	    return new ResponseEntity<>(userData.get(), HttpStatus.OK);
	  } else {
		  
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	  }
	}
	
//	@PutMapping("/user/{username}")
//	public ResponseEntity<User> updateUserByUsername(@PathVariable("username") String username){
//		Optional<User> userData = userRepository.findByUsername(username);
//    		
//    	
//    	System.out.println("lets update ******************************");
//    	
//    	return ResponseEntity.ok(userData.get());
//		
//	}
	
	// update profile
	@PutMapping("/userprofile/{username}")
	public ResponseEntity<User> updateEmployee(@PathVariable String username,@RequestBody User userDetails)
	{
		User user = userRepository.findByUsername(username)
				.orElseThrow(()->new ResourceNotFoundException("User not exist with username:"+username));
//		employee.setFirstname(employeeDetails.getFirstname());
//		employee.setLastname(employeeDetails.getLastname());
//		employee.setMail(employeeDetails.getMail());
		System.out.println("***********update fonction***************");
		user.setFirstName(userDetails.getFirstName());
		user.setLastName(userDetails.getLastName());
		user.setDateOfBirth(userDetails.getDateOfBirth());
		user.setAddress(userDetails.getAddress());
		user.setSexe(userDetails.getSexe());
		user.setTel(userDetails.getTel());
		
		user.setCity(userDetails.getCity());
		user.setCountry(userDetails.getCountry());
		
		user.setFacebook(userDetails.getFacebook());
		user.setWebsite(userDetails.getWebsite());
		user.setGit(userDetails.getGit());
		user.setAboutMe(userDetails.getAboutMe());
		user.setField(userDetails.getField());
		user.setStudyLevel(userDetails.getStudyLevel());
		user.setLanguages(userDetails.getLanguages());
		user.setProfession(userDetails.getProfession());
		
		
		
		
		User updatedUser = userRepository.save(user);
		return ResponseEntity.ok(updatedUser);
		
	}
	
	 @GetMapping("/userByEmail/{email}") 
	 public User
	  usernameByemail (@PathVariable("email") String email) 
	 { 
		 return 
		
		 this.userRepository.findByEmail(email).get(); }
	
	
	
	

}
