package org.ab.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ab.models.Mission;
import org.ab.models.Reclamation;
import org.ab.models.User;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.MissionRepository;
import org.ab.repository.ReclamationRepository;
import org.ab.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ReclamationController {
	@Autowired	
	MissionRepository missionRepository;
	
	@Autowired
	ReclamationRepository reclamationRepository;
	
	@Autowired
	UserRepository userRepository;
	
	
	@PostMapping("/addreclamation")
	// ResponseEntity<Reclamation> createmission2(@RequestBody Reclamation reclamation) {
		Reclamation createReclamation(@RequestBody Reclamation reclamation) {
		
		reclamation.setProcessed(false);
		Mission mission= this.missionRepository.findById(reclamation.getMissionID()).get();
		reclamation.setMission(mission);
		User user = this.userRepository.findByEmail(reclamation.getClaimerName()).get();
		reclamation.setClaimer(user);
		this.reclamationRepository.save(reclamation);
		mission.getClaimed().add(reclamation.getClaimerName());
		mission.setReclamations(this.reclamationRepository.findByMission(reclamation.getMissionID()));	
		this.missionRepository.save(mission);
		//return ResponseEntity.ok(reclamation);
		return reclamation;
	}
	
	@DeleteMapping("/deleteReclamation/{id}")
	public ResponseEntity<?> deleteClaimBymission(@PathVariable("id") String id) {
		
		
		Reclamation reclamation= this.reclamationRepository.findById(id).get();
		//Mission mission= this.missionRepository.findById(reclamation.getMissionID()).get();
		this.reclamationRepository.delete(reclamation);
		//mission.setClaimed(new ArrayList<String>());
		//this.missionRepository.save(mission);
		
		return ResponseEntity.ok(new MessageResponse("Claim Deleted Succefully"));
	}
	
	@GetMapping("/getreclamation/{id}")
	// ResponseEntity<Reclamation> createmission2(@RequestBody Reclamation reclamation) {
		Reclamation getReclamation(@PathVariable(value = "id") String reclamationId) {
		Reclamation reclamation=this.reclamationRepository.findById(reclamationId).get();
		return reclamation;
	}
	@GetMapping("/allReclamation")
	List<Reclamation> getAllReclamation(){
		
		return this.reclamationRepository.findAll();
	}
	
	@GetMapping("/allReclamationClaimer/{claimerName}")
	List<Reclamation> getReclamationByClaimer(@PathVariable(value = "claimerName") String claimerName){
		
		return this.reclamationRepository.findByclaimerName(claimerName);
	}
	
	
	@PostMapping("/TreatClaim")
	Reclamation TreatClaim(@RequestBody Reclamation reclamation){
		
			Reclamation claimToTreat= this.reclamationRepository.findById(reclamation.getId()).get();
			if(reclamation.getDecision().equals("ignore")){
				claimToTreat.setDecision(reclamation.getDecision());
				claimToTreat.setProcessed(true);
				this.reclamationRepository.save(claimToTreat);
				
				
				
			}
			else if(reclamation.getDecision().equals("block client")){
				LocalDateTime instance = LocalDateTime.now().plusDays(2);

				claimToTreat.setDecision(reclamation.getDecision());
				claimToTreat.setProcessed(true);

				
				User user=this.userRepository.findByEmail(claimToTreat.getMission().getClientemail()).get();
				user.setBlocked(true);
				user.setEndBlockDate(Date.from(instance.atZone(ZoneId.systemDefault()).toInstant()));
				this.userRepository.save(user);
				this.reclamationRepository.save(claimToTreat);
	
			}
			else if(reclamation.getDecision().equals("block avatar")){
				LocalDateTime instance = LocalDateTime.now().plusDays(2);

				claimToTreat.setDecision(reclamation.getDecision());
				claimToTreat.setProcessed(true);

				
				User user=this.userRepository.findByEmail(claimToTreat.getMission().getAvataremail()).get();
				user.setBlocked(true);
				user.setEndBlockDate(Date.from(instance.atZone(ZoneId.systemDefault()).toInstant()));
				this.userRepository.save(user);
				this.reclamationRepository.save(claimToTreat);
				
			}
		
		
		
		
		return claimToTreat;
	}
	
	@GetMapping("/verifreclamationexist/{id}/{claimer}")
	// ResponseEntity<Reclamation> createmission2(@RequestBody Reclamation reclamation) {
		Boolean verifReclamation(@PathVariable(value = "id") String missionId,@PathVariable(value = "claimer") String claimer) {
		Reclamation reclamation=this.reclamationRepository.findByMissionandClaimer(missionId, claimer);
		if(reclamation== null){return false;}
		else return true;
		
	}

}
