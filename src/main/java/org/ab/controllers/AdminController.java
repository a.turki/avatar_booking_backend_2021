package org.ab.controllers;

import java.util.ArrayList;
import java.util.List;

import org.ab.models.Country;
import org.ab.models.Mission;
import org.ab.models.Region;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.CountryRepository;
import org.ab.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class AdminController {
	@Autowired
	RegionRepository regionRepository;
	@Autowired
	CountryRepository countryRepository;
	
	@PostMapping("/addcountry")
	ResponseEntity<?> Addcountry(@RequestBody Country country) {
		if(!this.countryRepository.existsByName(country.getName()))
		{this.countryRepository.save(country);
	
		return ResponseEntity.ok(new MessageResponse("Country Added Succefully"));
		}
		else return ResponseEntity.ok(new MessageResponse("Country Name Already Exist"));
	}
	
	@PostMapping("/addregion")
	ResponseEntity<?> Addregion(@RequestBody Region region) {
		if((this.countryRepository.existsByName(region.getCountry()))&&(!this.regionRepository.existsByName(region.getName())))
		{
			this.regionRepository.save(region);
			return ResponseEntity.ok(new MessageResponse("Region Added Succefully"));
		}
		else if(!this.countryRepository.existsByName(region.getCountry())){
			return ResponseEntity.ok(new MessageResponse("CountryName doesn't exist"));

		}
		else if(this.regionRepository.existsByName(region.getName())){
			return ResponseEntity.ok(new MessageResponse("Region Name already exist"));

		}
		else return ResponseEntity.ok(new MessageResponse("*"));
		
		
	}
	
	@GetMapping("/allCountry")
	public List<Country> getAllCountry(){
		List<Country> country = this.countryRepository.findAll();
		return country;
	}
	@GetMapping("/allCountryRegions/{country}")
	public List<Region> getAllCountryRegions(@PathVariable(value = "country") String country){
		List<Region> region = this.regionRepository.getRegions(country);
		return region;
	}
	
	@GetMapping("/allRegions")
	public List<Region> getAllRegions(){
		List<Region> region = this.regionRepository.findAll();
		return region;
	}
	
	
}
