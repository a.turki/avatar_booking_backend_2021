package org.ab.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.ab.models.Mission;
import org.ab.models.Offer;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.MissionRepository;
import org.ab.repository.OfferRepository;
import org.ab.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class OfferController {
	@Autowired	
	MissionRepository missionRepository;
	@Autowired	
	OfferRepository offerRepository;
	@Autowired	
	UserRepository userRepository;
	
	@PostMapping("/makeanoffer")
	ResponseEntity<?> makeAnOffer(@RequestBody Offer offer) {
		LocalDateTime instance = LocalDateTime.now();
		Optional<Mission> retrievedMission = missionRepository.findById(offer.getMissionId());
		System.out.println("mission offer :" + retrievedMission.get().getTitle());
		offer.setAvatar(this.userRepository.findByEmail(offer.getAvatarMail()).get());
		offer.setAccepeted(false);
		offer.setState("pending");
		offer.setDate(Date.from(instance.atZone(ZoneId.systemDefault()).toInstant()));
		this.offerRepository.save(offer);
		
		//retrievedMission.get().getListavatarOffer().add(offer);
		retrievedMission.get().getListavatarInterested().add(offer.getAvatarMail());
		retrievedMission.get().getOffers().add(offer);
		
		this.missionRepository.save(retrievedMission.get());
		System.out.println("makedd offer " + retrievedMission.get().getOffers());
		System.out.println("*********************************offer added*********************************");
		return ResponseEntity.ok(new MessageResponse("Offer Added Succefully"));
	}
	
	
	@PutMapping("/updateOffer/{id}")
	public ResponseEntity<?> updateMission(@PathVariable(value = "id") String id,
			 @RequestBody Offer offer){
		Offer retrievedOffer = offerRepository.findById(id).get();
		
		
		retrievedOffer.setPrice(offer.getPrice());
		
		return ResponseEntity.ok(new MessageResponse("Offer Updated Succefully"));
	}
	
	
	
	
	@PostMapping("/cancelanoffer/{id}/{avataremail}")
	ResponseEntity<?> cancelAnOffer(@PathVariable("id") String id,
			@PathVariable(value = "avataremail") String avatarMail) {
		
		Offer retrievedOffer = offerRepository.findOfferByMissionIDandAvatarMail(id, avatarMail);
		Optional<Mission> retrievedMission = missionRepository.findById(retrievedOffer.getMissionId());
		//retrievedMission.get().getListavatarCandidat().add(retrievedOffer.get().getAvatarMail());
		int index=retrievedMission.get().getListavatarInterested().indexOf(retrievedOffer.getAvatarMail());
		retrievedMission.get().getListavatarInterested().remove(index);
		
		//retrievedMission.get().getOffers().remove(retrievedOffer);
		this.offerRepository.delete(retrievedOffer);
		
		List<Offer> o=this.offerRepository.getOffersByMissionId(id);
		retrievedMission.get().setOffers(o);
		
		this.missionRepository.save(retrievedMission.get());
		System.out.println("*********************************offer Canceled*********************************");
		return ResponseEntity.ok(new MessageResponse("Offer Canceled Succefully"));
	}
	
	
	@PostMapping("/acceptoffer/{id}")
	ResponseEntity<?> AcceptOffer(@PathVariable("id") String id) {
		Optional<Offer> retrievedOffer = offerRepository.findById(id);
		 
		String MissionId=retrievedOffer.get().getMissionId();
		
		Optional<Mission> retrievedMission = missionRepository.findById(MissionId);
		
		List<Offer>OffersByMission= offerRepository.getOffersByMissionId(MissionId);
		
		
		
		
		
		for (int i=0;i< OffersByMission.size();i++)
		{
			if(OffersByMission.get(i).getId().equals(id))
			{
				OffersByMission.get(i).setAccepeted(true);
				OffersByMission.get(i).setState("accepted");
				
				retrievedMission.get().setAvataremail(retrievedOffer.get().getAvatarMail());
				retrievedMission.get().setPrice(retrievedOffer.get().getPrice());
				retrievedMission.get().setState("accepted");
				retrievedMission.get().setProgress("undone");
				
				this.missionRepository.save(retrievedMission.get());
				this.offerRepository.save(OffersByMission.get(i));
				
			}else{
				OffersByMission.get(i).setState("declined");
				//retrievedMission.get().getListavatarCandidat().remove(retrievedOffer.get().getAvatarMail());
				//retrievedMission.get().getListavatarDeclined().add(retrievedOffer.get().getAvatarMail());
				retrievedMission.get().getListavatarDeclined().remove(OffersByMission.get(i).getAvatarMail());
				this.missionRepository.save(retrievedMission.get());
				this.offerRepository.save(OffersByMission.get(i));
				
				
				
			}
		}
		
		
		
	
		System.out.println("*********************************offer accepted*********************************");
		return ResponseEntity.ok(new MessageResponse("Offer "+ id +" Accepted by " + retrievedOffer.get().getAvatarMail()));
	}
	@PostMapping("/declineoffer/{id}")
	ResponseEntity<?> DeclineOffer(@PathVariable("id") String id) {
		Optional<Offer> retrievedOffer = offerRepository.findById(id);
		Optional<Mission> retrievedMission = missionRepository.findById(retrievedOffer.get().getMissionId());
		retrievedOffer.get().setState("declined");
		retrievedMission.get().getListavatarDeclined().add(retrievedOffer.get().getAvatarMail());
		
		this.missionRepository.save(retrievedMission.get());
		this.offerRepository.save(retrievedOffer.get());
	
		System.out.println("*********************************offer declined*********************************");
		return ResponseEntity.ok(new MessageResponse("Offer "+ id +" Declined by " + retrievedOffer.get().getAvatarMail()));
	}
	
	 @GetMapping("/OffersByMission/{missionId}")
		public List<Offer> getMissionOffers(@PathVariable(value = "missionId") String missionId)
	  {
		 List<Offer>OffersByMission= offerRepository.getOffersByMissionId(missionId);
		 return OffersByMission;
		 
	  }
	

}
