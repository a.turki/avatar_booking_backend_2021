package org.ab.controllers;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.ab.models.Photo;
import org.ab.models.User;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.PhotoRepository;
import org.ab.repository.UserRepository;
import org.ab.services.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;


import lombok.AllArgsConstructor;


@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class PhotoController {

	@Autowired	
	PhotoRepository photoRepository;
	MongoTemplate mongoTemplate;
	
	
	@PostMapping("/upload/{imageName}")
	public ResponseEntity<Photo> uplaodImage(@RequestParam("imageFile") MultipartFile file,@PathVariable("imageName") String imageName) throws IOException {
		String username;
		Boolean exists=photoRepository.existsByTheuser(imageName);
		System.out.println("exits photo ="+exists);
		if(exists){
			final Optional<Photo> retrievedImage = photoRepository.findByTheuser(imageName);
			Photo img = new Photo(retrievedImage.get().getId(),retrievedImage.get().getName(), retrievedImage.get().getType(),
					decompressZLib(retrievedImage.get().getPicByte()),retrievedImage.get().getTheuser());
			System.out.println("photo getted yes");
			 
			photoRepository.delete(img);
			
			
			System.out.println("photo DELETED");
			
		}
		
		
		System.out.println("Original Image Byte Size - " + file.getBytes().length);
		Photo img = new Photo(file.getOriginalFilename(), file.getContentType(),
				compressZLib(file.getBytes()),imageName);
		photoRepository.save(img);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	@GetMapping(path = { "/delete/{imageName}" })
	public ResponseEntity<?> deleteImage(@PathVariable("imageName") String imageName) throws IOException {
		
		final Optional<Photo> retrievedImage = photoRepository.findByTheuser(imageName);
		Photo img = new Photo(retrievedImage.get().getId(),retrievedImage.get().getName(), retrievedImage.get().getType(),
				decompressZLib(retrievedImage.get().getPicByte()),retrievedImage.get().getTheuser());
		System.out.println("photo getted yes");
		 
		photoRepository.delete(img);
		
		
		System.out.println("photo DELETED");
		return  ResponseEntity.ok(new MessageResponse("photo of "+imageName + " deleted"));
	}
	
	
	@GetMapping(path = { "/get/{imageName}" })
	public Photo getImage(@PathVariable("imageName") String imageName) throws IOException {
		Boolean exists=photoRepository.existsByTheuser(imageName);
		System.out.println("exits photo ="+exists);
		final Optional<Photo> retrievedImage = photoRepository.findByTheuser(imageName);
		Photo img = new Photo(retrievedImage.get().getName(), retrievedImage.get().getType(),
				decompressZLib(retrievedImage.get().getPicByte()),retrievedImage.get().getTheuser());
		System.out.println("photo getted yes");
		return img;
	}

	// compress the image bytes before storing it in the database
	public static byte[] compressZLib(byte[] data) {
		Deflater deflater = new Deflater();
		deflater.setInput(data);
		deflater.finish();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		try {
			outputStream.close();
		} catch (IOException e) {
		}
		System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);

		return outputStream.toByteArray();
	}

	// uncompress the image bytes before returning it to the angular application
	public static byte[] decompressZLib(byte[] data) {
		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		try {
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
		} catch (IOException ioe) {
		} catch (DataFormatException e) {
		}
		return outputStream.toByteArray();
	}
}
