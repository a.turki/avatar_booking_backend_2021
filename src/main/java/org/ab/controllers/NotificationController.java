package org.ab.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.ab.email.EmailSender;
import org.ab.models.Mission;
import org.ab.models.Notification;
import org.ab.models.Offer;
import org.ab.models.User;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.NotificationRepository;
import org.ab.repository.RoleRepository;
import org.ab.repository.UserRepository;
import org.ab.security.jwt.JwtUtils;
import org.ab.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@CrossOrigin("*")
@RequestMapping("/api")
public class NotificationController {
	
	@Autowired
	NotificationRepository notificationRepository;
	@Autowired
	UserRepository userRepository;
	
	private final EmailSender emailSender;
	
	@PostMapping("/notify")
	ResponseEntity<?> makeNotification(@RequestBody Notification notification) {
		LocalDateTime instance = LocalDateTime.now();
		notification.setSeen(false);
		notification.setSentAt(Date.from(instance.atZone(ZoneId.systemDefault()).toInstant()));
		this.notificationRepository.save(notification);
		// User userRecipient=userRepository.findByUsername(notification.getRecipientUsername()).get();
		User userRecipient=userRepository.findByEmail(notification.getRecipientUsername()).get();
		
		
		//emailSender.send(userRecipient.getEmail(),buildNotificationEmail(notification.getSenderUsername()));
		emailSender.sendNotif(userRecipient.getEmail(), buildNotificationEmail(notification));
		System.out.println("*********************************notification added*********************************");
		return ResponseEntity.ok(new MessageResponse("Notification added Succefully"));
	}
	
	  @PutMapping("/CheckNotification/{id}")
		public ResponseEntity<?> checkNotification(@PathVariable(value = "id") String id)  {
		Optional<Notification> retrievedNotification = notificationRepository.findById(id);
		LocalDateTime instance = LocalDateTime.now();
		
		retrievedNotification.get().setSeen(true);
		retrievedNotification.get().setSeenAt(Date.from(instance.atZone(ZoneId.systemDefault()).toInstant()));
		
		
		notificationRepository.save(retrievedNotification.get());
		System.out.println("Notification Checked");
		return ResponseEntity.ok(retrievedNotification);

	  }
	  
		//all my Notifications
		@GetMapping("/allMyNotifications/{recipientUsername}")
		public List<Notification> getAllMyNotifications(@PathVariable(value = "recipientUsername") String recipientUsername)
		{
			List<Notification> myNotifications=this.notificationRepository.getMyNotifications(recipientUsername);
	
					return myNotifications;
		
		}
		
		//all my unchecked Notifications
		@GetMapping("/MyUnseenNotifications/{recipientUsername}")
		public List<Notification> getAllMyUnseenNotifications(@PathVariable(value = "recipientUsername") String recipientUsername)
		{
			List<Notification> myNotifications=this.notificationRepository.getMyUnseenNotifications(recipientUsername, false);
	
					return myNotifications;
		
		}
		private String buildNotificationEmail(Notification notification)
	     {
			// String MailContent="test notif mail :"+ name+" accepted your mission";
			String MailContent= notification.getSenderUsername() + notification.getType();
	        return MailContent;
	    }
	
	
}
