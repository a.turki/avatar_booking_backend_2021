package org.ab.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ab.models.ERole;
import org.ab.models.Mission;
import org.ab.models.Offer;
import org.ab.models.Photo;
import org.ab.models.Reclamation;
import org.ab.models.Role;
import org.ab.models.User;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.MissionRepository;
import org.ab.repository.PhotoRepository;
import org.ab.repository.RoleRepository;
import org.ab.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import ch.qos.logback.core.net.SyslogOutputStream;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class MissionController {
	@Autowired	
	MissionRepository missionRepository;
	@Autowired	
	UserRepository userRepository;
	@Autowired	
	RoleRepository roleRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	
	@PostMapping("/addmission")
	ResponseEntity<?> createmission(@RequestBody Mission mission) {
		List<String> declined=new ArrayList<String>();
		List<String> intrested=new ArrayList<String>();
		
		mission.setListavatarInterested(intrested);
		
		mission.setListavatarDeclined(declined);
		mission.setState("pending");
		mission.setProgress("pending");
		mission.setRated(false);
		mission.setExpired(false);
		this.missionRepository.save(mission);
		System.out.println("*********************************mission added*********************************");
		System.out.println("*********************************mission expiration " + mission.getExpiredate());

		return ResponseEntity.ok(new MessageResponse("Mission Added Succefully"));
	}
	@PostMapping("/addmission2")
	ResponseEntity<Mission> createmission2(@RequestBody Mission mission) {
		List<String> declined=new ArrayList<String>();
		List<String> intrested=new ArrayList<String>();
		List<Offer> offers=new ArrayList<Offer>();
		List<Reclamation> reclamations=new ArrayList<Reclamation>();
		List<String> claimed =new ArrayList<String>();
		
		//mission.setAvatar(this.userRepository.findByEmail(mission.getAvataremail()).get());
		// mission.setClient(this.userRepository.findByEmail(mission.getClientemail()).get());
		mission.setListavatarInterested(intrested);
		mission.setReclamations(reclamations);
		mission.setClaimed(claimed);
		mission.setOffers(offers);
		mission.setListavatarDeclined(declined);
		mission.setState("pending");
		mission.setProgress("pending");
		mission.setRated(false);
		mission.setExpired(false);
		this.missionRepository.save(mission);
		
		System.out.println("*********************************mission added*********************************");
		System.out.println("*********************************mission expiration " + mission.getExpiredate());
		
		User client = this.userRepository.findByEmail(mission.getClientemail()).get();
		 // List <Mission> m = new ArrayList<Mission>();
		// m=this.missionRepository.getClientMissions(mission.getClientemail());
		// m.add(mission);
		// client.getAvatarMissions().add(mission);
		
		 client.getClientMissions().add(mission);
		 this.userRepository.save(client);
		
		return ResponseEntity.ok(mission);
	}
	
	@DeleteMapping("/deleteMissionById/{id}")
	public ResponseEntity<?> deletemission(@PathVariable("id") String id) {
		Optional<Mission> retrievedMission = missionRepository.findById(id);
		
		/*Mission todeletemission = new Mission(retrievedMission.get().getId(),
											  retrievedMission.get().getTitle(),
											  retrievedMission.get().getDescription(),
											  retrievedMission.get().getPrice(),
											  retrievedMission.get().getAddress(),
											  retrievedMission.get().getCountry(),
											  retrievedMission.get().getRegion(),
											  retrievedMission.get().getDate(),
											  retrievedMission.get().getExpiredate(),
											  retrievedMission.get().getTime(),
											  retrievedMission.get().getDuration(),
											  retrievedMission.get().getPreference(),
											  retrievedMission.get().getClientemail(),
											  retrievedMission.get().getType(),
											  retrievedMission.get().getListavatarCandidat(),
											  retrievedMission.get().getListavatarDeclined(),
											  retrievedMission.get().getListavatarInterested(),
											  retrievedMission.get().getAvataremail(),
											  retrievedMission.get().getState(),
											  retrievedMission.get().getProgress(),
											  retrievedMission.get().isRated(),
											  retrievedMission.get().isExpired(),
											  retrievedMission.get().getAvatar(),
											  retrievedMission.get().getClient(),
											  retrievedMission.get().getReclamations());*/
		Mission todeletemission =retrievedMission.get();
		System.out.println("Mission getted ");
		System.out.println(todeletemission.getClientemail()+"'missions will be deleted");
		

		this.missionRepository.delete(todeletemission);
		System.out.println("*********************************mission deleted*********************************");
		 User client = this.userRepository.findByEmail(retrievedMission.get().getClientemail()).get();
		 List <Mission> m = new ArrayList<Mission>();
		 m=this.missionRepository.getClientMissions(retrievedMission.get().getClientemail());
		 client.setClientMissions(m);
		this.userRepository.save(client);

		
		 //User client = this.userRepository.findByEmail(retrievedMission.get().getClientemail()).get();
		 //client.getClientMissions().remove(retrievedMission.get());
		
		return ResponseEntity.ok(new MessageResponse("Mission Deleted Succefully"));
	}
	
	  @PutMapping("/updateMission/{id}")
		public ResponseEntity<?> updateMission(@PathVariable(value = "id") String id,
													 @RequestBody Mission mission)  {
		  Optional<Mission> retrievedMission = missionRepository.findById(id);
			
		  
		  retrievedMission.get().setAddress(mission.getAddress());
		  retrievedMission.get().setClientemail(mission.getClientemail());
		  retrievedMission.get().setDate(mission.getDate());
		  retrievedMission.get().setDescription(mission.getDescription());
		  retrievedMission.get().setDuration(mission.getDuration());
		  retrievedMission.get().setPreference(mission.getPreference());
		  retrievedMission.get().setPrice(mission.getPrice());
		  retrievedMission.get().setRegion(mission.getRegion());
		  retrievedMission.get().setTime(mission.getTime());
		  retrievedMission.get().setTitle(mission.getTitle());
		  //retrievedMission.get().setState(mission.getState());
		  retrievedMission.get().setAvataremail(mission.getAvataremail());
		  retrievedMission.get().setListavatarCandidat(mission.getListavatarCandidat());
		  retrievedMission.get().setListavatarDeclined(mission.getListavatarDeclined());
		  System.out.println(retrievedMission.get().getState());

			final Mission updatedMission=retrievedMission.get();
			missionRepository.save(updatedMission);
			 User client = this.userRepository.findByEmail(retrievedMission.get().getClientemail()).get();
			 List <Mission> m = new ArrayList<Mission>();
			 m=this.missionRepository.getClientMissions(retrievedMission.get().getClientemail());
			 client.setClientMissions(m);
			this.userRepository.save(client);
			System.out.println("SUCCESSFULLY UPDATED");
			return ResponseEntity.ok(updatedMission);

		}
	  	//all missions 
		@GetMapping("/allMissions")
		public List<Mission> getAllMissions(){
			List<Mission> missions = this.missionRepository.findAll();
			System.out.println("list of missions getted");
			return missions;
		}
		//all my missions 
		@GetMapping("/allMyMissions/{clientemail}")
		public List<Mission> getAllMyMissions(@PathVariable(value = "clientemail") String clientMail)
		{
			List<Mission> myMissions=new ArrayList<Mission>();
			List<Mission> ct = this.missionRepository.findAll();
			String progr;
			boolean test;
				for (int i=0;i< ct.size();i++)
		{//System.out.println(ct.get(i).getProgress());
					if( ct.get(i).getClientemail().equals(clientMail) )
					{
						
						myMissions.add(ct.get(i));
					}
			}
					return myMissions;
		
		}
		
		// get all mission's avatars
		@GetMapping("/allMissionAvatars/{id}")
		public List<User> getAllMissionAvatars(@PathVariable("id") String id){
			List<User>avatars=new ArrayList<User>();
			Mission mission=this.missionRepository.findById(id).get();
			List<String>ct=mission.getListavatarCandidat();
			for (int i = 0; i < ct.size();i++)
			  {
				System.out.println(ct.get(i));
				User avatar=this.userRepository.findByEmail(ct.get(i)).get();
				avatars.add(avatar);
				
				
				
			  }
			
			
			
			
			return avatars;
		}
		
		
		
		
		@GetMapping("/allAvatar")
		public List<User> getAllAvatar(){
			Optional<Role> E = this.roleRepository.findByName(ERole.ROLE_AVATAR);
			List<User>avatars=new ArrayList<User>();
			List<User> ct = this.userRepository.findAll();
			List<String>candidat=new ArrayList<String>();
			
			
			for (int i = 0; i < ct.size();i++)
			  {
				for(Role s : ct.get(i).getRoles())
					  if(s.getName().equals(ERole.ROLE_AVATAR))
				        {avatars.add(ct.get(i));
				        	
						  System.out.println(s.getName());}
			  }
			System.out.println("************list of avatar getted****************");
			
			for (int j = 0; j < avatars.size();j++){
				candidat.add(avatars.get(j).getEmail());
			}
			System.out.println("string candidat"+candidat);

				return avatars;
		}
		
		@GetMapping("/allAvatarStr")
		public List<String> getAllAvatarStr(){
			Optional<Role> E = this.roleRepository.findByName(ERole.ROLE_AVATAR);
			List<User>avatars=new ArrayList<User>();
			List<User> ct = this.userRepository.findAll();
			List<String>candidats=new ArrayList<String>();
			
			
			for (int i = 0; i < ct.size();i++)
			  {
				for(Role s : ct.get(i).getRoles())
					  if(s.getName().equals(ERole.ROLE_AVATAR))
				        {avatars.add(ct.get(i));
				        	
						  System.out.println(s.getName());}
			  }
			System.out.println("************list of avatar getted****************");
			
			for (int j = 0; j < avatars.size();j++){
				candidats.add(avatars.get(j).getEmail());
			}
			System.out.println("string candidat"+candidats);

				return candidats;
		}
		@GetMapping("/allAvatarStr/{region}")
		public List<String> getAllAvatarStrByRegion(@PathVariable("region") String region){
			List<User> ListeAvatar = this.userRepository.allAvatarByRegion(region, true);
			List<String>candidats=new ArrayList<String>();
			for (int i = 0; i < ListeAvatar.size();i++)
			  {
				candidats.add(ListeAvatar.get(i).getEmail());
				
			  }
			
			
			
			
			return candidats;
		}
		
		@GetMapping("/allAvatarUsernameStr")
		public List<String> getAllAvatarUsernameStr(){
			Optional<Role> E = this.roleRepository.findByName(ERole.ROLE_AVATAR);
			List<User>avatars=new ArrayList<User>();
			List<User> ct = this.userRepository.findAll();
			List<String>candidats=new ArrayList<String>();
			
			
			for (int i = 0; i < ct.size();i++)
			  {
				for(Role s : ct.get(i).getRoles())
					  if(s.getName().equals(ERole.ROLE_AVATAR))
				        {avatars.add(ct.get(i));
				        	
						  System.out.println(s.getName());}
			  }
			System.out.println("************list of avatar getted****************");
			
			for (int j = 0; j < avatars.size();j++){
				candidats.add(avatars.get(j).getUsername());
			}
			System.out.println("string candidat"+candidats);

				return candidats;
		}
		
		
		
		 @GetMapping("findMissionById/{id}") 
		 public Optional<Mission>
		  missionById (@PathVariable("id") String id) 
		 { 
			 Calendar cal = Calendar.getInstance();
			 cal.setTime(this.missionRepository.findById(id).get().getDate());
			 int month = cal.get(Calendar.MONTH)+1; // 5
			 System.out.println("le month : " +month);
			 return 
			this.missionRepository.findById(id); 
			 }
	
		 
		 // acceptMission Method 
	  @GetMapping("acceptMission/{id}/{avataremail}")
		public ResponseEntity<Mission> AcceptMission(@PathVariable(value = "id") String missionId,
				@PathVariable(value = "avataremail") String avatarMail)  
	  {
			
		    Optional<Mission> acceptedMission = missionRepository.findById(missionId);
		    acceptedMission.get().getListavatarCandidat().remove(avatarMail);
		    acceptedMission.get().setAvataremail(avatarMail);
		  	acceptedMission.get().setState("accepted");
		  	acceptedMission.get().setProgress("undone");
			this.missionRepository.save(acceptedMission.get());
			System.out.println("Mission accepted");
			return ResponseEntity.ok(acceptedMission.get());

		}
	  
	  // decline mission
	  @GetMapping("declineMission/{id}/{avataremail}")
		public ResponseEntity<Mission> DeclineMission(@PathVariable(value = "id") String missionId,
				@PathVariable(value = "avataremail") String avatarMail)  
	  {
		    Optional<Mission> declineMission = missionRepository.findById(missionId);
		    boolean a=declineMission.get().getListavatarCandidat().contains(avatarMail);
		    boolean b=declineMission.get().getListavatarDeclined().contains(avatarMail);
		    if(a && !b)
		    {
		    declineMission.get().getListavatarDeclined().add(avatarMail);
		    declineMission.get().getListavatarCandidat().remove(avatarMail);
		    this.missionRepository.save(declineMission.get());
		    }
		    System.out.println("Mission declined");
			return ResponseEntity.ok(declineMission.get());

	}
	  
	  // Mission list of avatar candidats
	  @GetMapping("/MissionAvatars/{id}")
		public List<User> getMissionAvatars(@PathVariable(value = "id") String missionId)
	  {
			List<User>avatars=new ArrayList<User>();
			Optional<Mission> Mission = missionRepository.findById(missionId);
			List<String>ct=Mission.get().getListavatarCandidat();
			for (int i=0;i< ct.size();i++)
			{
				User avatar =new User();
				avatar=userRepository.findByEmail(ct.get(i)).get();
				avatars.add(avatar);
				System.out.println(ct.get(i));
			}
			return avatars;
			
		}
	
	  
	  // GET AVATAR OPPORTUNITIES
	  @GetMapping("/MissionOpportunities/{avataremail}")
			public List<Mission> getMissionOpportunities(@PathVariable(value = "avataremail") String avatarMail)
		  {
				List<Mission>missionOpportunities=new ArrayList<Mission>();
				List<Mission> ct =this.missionRepository.findAll();
				
				//Query query1=new Query();
		    	//query1.addCriteria(Criteria.where("email").is(recoverRequest.getEmail()));
		    	//User ct=mongoTemplate.findOne(query1, User.class);
				
				for (int i=0;i< ct.size();i++)
				{
					//ct.get(i).getListavatarCandidat()
					if(ct.get(i).getState().equals("pending"))
					
					{
						//System.out.println("*******************************************test");
						for(int j=0;j<ct.get(i).getListavatarCandidat().size();j++)
						{
							//System.out.println(ct.get(i).getListavatarCandidat().get(j));
							if(ct.get(i).getListavatarCandidat().get(j).equals(avatarMail))
							{
								System.out.println("---------------------");
								missionOpportunities.add(ct.get(i));
								
							}
						}
						
					}
					
				}
				return missionOpportunities;
				//return ct;
				
			}
	  
	  
	 // get my undone mission
		@GetMapping("/myundonemission/{avataremail}")
		public List<Mission> getMyUndoneMissions(@PathVariable(value = "avataremail") String avatarMail)
		{
			List<Mission> undoneMissions=new ArrayList<Mission>();
			List<Mission> ct = this.missionRepository.findAll();
			String progr;
			boolean test;
				for (int i=0;i< ct.size();i++)
		{//System.out.println(ct.get(i).getProgress());
					if(ct.get(i).getProgress()!= null && ct.get(i).getProgress().equals("undone") && ct.get(i).getAvataremail().equals(avatarMail) )
					{
						progr=ct.get(i).getProgress();
						test=progr.equals("undone");
						System.out.println("1312: "+test);
						undoneMissions.add(ct.get(i));
					}

		

	
}
			System.out.println("list of undone missions getted ");
			return undoneMissions;
		}
		 // start Mission Method 
	  @GetMapping("startMission/{id}")
		public ResponseEntity<Mission> StartMission(@PathVariable(value = "id") String missionId)  
	  {
			
		    Optional<Mission> MissionToStart = missionRepository.findById(missionId);
		    MissionToStart.get().setProgress("doing");
			this.missionRepository.save(MissionToStart.get());
			System.out.println("Mission Started");
			return ResponseEntity.ok(MissionToStart.get());

		}
		 // End Mission Method 
	  @GetMapping("endMission/{id}")
		public ResponseEntity<Mission> EndMission(@PathVariable(value = "id") String missionId)  
	  {
			
		    Optional<Mission> MissionToEnd = missionRepository.findById(missionId);
		    if (MissionToEnd.get().getProgress().equals("doing"))
		    {MissionToEnd.get().setProgress("done");}
			this.missionRepository.save(MissionToEnd.get());
			System.out.println("Mission Ended");
			return ResponseEntity.ok(MissionToEnd.get());

		}
	  
		 // get my doing mission
			@GetMapping("/mydoingmission/{avataremail}")
			public List<Mission> getMyDoingMissions(@PathVariable(value = "avataremail") String avatarMail)
			{
				List<Mission> doingMissions=new ArrayList<Mission>();
				List<Mission> ct = this.missionRepository.findAll();
				String progr;
				boolean test;
	for (int i=0;i< ct.size();i++)
	{
		if(ct.get(i).getProgress()!= null && ct.get(i).getProgress().equals("doing") && ct.get(i).getAvataremail().equals(avatarMail) )
		{
		progr=ct.get(i).getProgress();
		test=progr.equals("doing");
		System.out.println("1312: "+test);
		doingMissions.add(ct.get(i));
		}

			

		
	}
				System.out.println("list of doing missions getted ");
				return doingMissions;
		  }
			
	//***********************
			 // get my done mission
			@GetMapping("/mydonemission/{avataremail}")
			public List<Mission> getMyDoneMissions(@PathVariable(value = "avataremail") String avatarMail)
			{
				List<Mission> doneMissions=new ArrayList<Mission>();
				List<Mission> ct = this.missionRepository.findAll();
				String progr;
				boolean test;
	for (int i=0;i< ct.size();i++)
	{
		if(ct.get(i).getProgress()!= null && ct.get(i).getProgress().equals("done") && ct.get(i).getAvataremail().equals(avatarMail) )
		{
		progr=ct.get(i).getProgress();
		test=progr.equals("done");
		System.out.println("1312: "+test);
		doneMissions.add(ct.get(i));
		}

			

		
	}
				System.out.println("list of done missions getted ");
				return doneMissions;
		  }
			
			@GetMapping("/avatarfinishedmission/{avataremail}")
			public List<Mission> AvatarMissions(@PathVariable(value = "avataremail") String avatarMail)
			{
				List<Mission> ct=this.missionRepository.getAvatarMissions(avatarMail,true);
				
				return ct;
				
			}
			
			@GetMapping("/distinctProgress")
	public List<String> SelectDistinctProgress()
	{
		List<String> categoryList = new ArrayList<>();
	    MongoCollection mongoCollection = mongoTemplate.getCollection("mission");
	    DistinctIterable distinctIterable = mongoCollection.distinct("progress",String.class);
	    MongoCursor cursor = distinctIterable.iterator();
	    while (cursor.hasNext()) {
	        String category = (String)cursor.next();
	        categoryList.add(category);
	    }
	    return categoryList;
		
	}
			
		@GetMapping("/countProgress/{progress}")
		public Number countProgress(@PathVariable(value = "progress") String progress){
				
				
		return this.missionRepository.countProgress(progress);
		}
		
		
		
		
		
		@GetMapping("/distinctCountry")
		public List<String> SelectDistinctCountry()
		{
			List<String> categoryList = new ArrayList<>();
		    MongoCollection mongoCollection = mongoTemplate.getCollection("mission");
		    DistinctIterable distinctIterable = mongoCollection.distinct("country",String.class);
		    MongoCursor cursor = distinctIterable.iterator();
		    while (cursor.hasNext()) {
		        String category = (String)cursor.next();
		        categoryList.add(category);
		    }
		    return categoryList;
			
		}
		
		
		
		
		@GetMapping("/countCountry/{country}")
		public Number countCountry(@PathVariable(value = "country") String country){
				
				
		return this.missionRepository.countCountry(country);
		}
			
		
			
	
}
