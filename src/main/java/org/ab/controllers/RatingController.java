package org.ab.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.ab.models.Mission;
import org.ab.models.Rating;
import org.ab.models.User;
import org.ab.payload.response.MessageResponse;
import org.ab.repository.MissionRepository;
import org.ab.repository.RatingRepository;
import org.ab.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RatingController {
	@Autowired	
	RatingRepository ratingRepository;
	@Autowired	
	MissionRepository missionRepository;
	@Autowired	
	UserRepository userRepository;
	
	@PostMapping("/ratemission/{missionid}")
	ResponseEntity<?> ratemission(@RequestBody Rating rating,
								  @PathVariable(value = "missionid") String missionid) {
		
		
		Optional<Mission> MissiontoRate = missionRepository.findById(missionid);
		boolean existMission=missionRepository.existsById(missionid);
		boolean exist = ratingRepository.existsByMissionId(missionid);
		float avgRate;
		Optional<User> avatar = userRepository.findByEmail(MissiontoRate.get().getAvataremail());
		
		if((!exist) && (existMission))
		{	
			rating.setMissionId(MissiontoRate.get().getId());
			rating.setClientEmail(MissiontoRate.get().getClientemail());
			rating.setAvatarEmail(MissiontoRate.get().getAvataremail());
		this.ratingRepository.save(rating);
		MissiontoRate.get().setRated(true);
		this.missionRepository.save(MissiontoRate.get());
		avgRate=RateAVG(MissiontoRate.get().getAvataremail());
		avatar.get().setRating(avgRate);
		avatar.get().setRatingcount(avatar.get().getRatingcount() + 1);
		this.userRepository.save(avatar.get());
		
		System.out.println("*********************************mission rated*********************************");
		
		
		
		return ResponseEntity.ok(new MessageResponse("Mission Rated Succefully"));
		}
	
	
		else if(MissiontoRate.get().isRated()) {
			System.out.println("*********************************mission alerady rated*********************************");

			return ResponseEntity.ok(new MessageResponse("Mission Already Rated"));
		}
		else  {
			return ResponseEntity.ok(new MessageResponse("Mission Does not exist"));
		}
		 
	}
	
	
	@GetMapping("/ratedmission/{missionid}")
	 boolean isRated(@PathVariable(value = "missionid") String missionid) {

		boolean existMission=missionRepository.existsById(missionid);
		boolean exist = ratingRepository.existsByMissionId(missionid);
		
		
		return exist;
	
		 
	}
	
	@GetMapping("/AvatarRates/{avatarmail}")
	List<Rating> RateTotal(@PathVariable(value="avatarmail")String avatarEmail){
		//float totale = 0 ;
		
		//List<Mission> myMissions=new ArrayList<Mission>();
		//List<Mission> ct = this.missionRepository.findAll();
		List<Rating> ratings=new ArrayList<Rating>();
		List<Rating> ct=this.ratingRepository.findAll();
		
		for (int i=0;i< ct.size();i++)
		{//System.out.println(ct.get(i).getProgress());
					if( ct.get(i).getAvatarEmail().equals(avatarEmail) )
					{
						
						ratings.add(ct.get(i));
					}
			}
		
		
		
		
		return ratings;
	}
	
	@GetMapping("/AvatarRatesAVG/{avatarmail}")
	float RateAVG (@PathVariable(value="avatarmail")String avatarEmail){
		float ratesAVG = 0f;
		List<Rating> ratings= this.ratingRepository.getRates(avatarEmail);
		for (int i=0;i< ratings.size();i++)
		{
					if( ratings.get(i).getAvatarEmail().equals(avatarEmail) )
					{
						System.out.println(ratings.get(i).getRate());
						
						ratesAVG = ratesAVG +ratings.get(i).getRate();
					}
			}
		
		
		return ratesAVG/ratings.size();
		
	}
	
	@GetMapping("/AvatarRatesCount/{avatarmail}")
	int RateCount (@PathVariable(value="avatarmail")String avatarEmail){
		List<Rating> ratings= this.ratingRepository.getRates(avatarEmail);
		Optional<User> avatar = userRepository.findByEmail(avatarEmail);
		avatar.get().setRatingcount(ratings.size());
		this.userRepository.save(avatar.get());

		
		
		return ratings.size();
		
	}
	
}
