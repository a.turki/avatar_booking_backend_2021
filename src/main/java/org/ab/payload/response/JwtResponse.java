package org.ab.payload.response;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tokens")
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	@Id
	private String id;
	private String username;
	private String email;
	private List<String> roles;
	private boolean enabled;

/*	public JwtResponse(String token, String type, String username, String email, List<String> roles) {
		//super();
		this.token = token;
		this.type = type;
		this.username = username;
		this.email = email;
		this.roles = roles;
	}
*/
	public JwtResponse(String accessToken,
						String id,
						String username,
						String email,
						List<String> roles,
						boolean enabled) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.enabled=enabled;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getRoles() {
		return roles;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
}
