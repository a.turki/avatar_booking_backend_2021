package org.ab.ws.configuration;

import org.ab.ws.handler.ChatWebSocketHandler;
import org.ab.ws.handler.NotificationWebSocketHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketNotificationConfiguration implements WebSocketConfigurer {
private final static String NOTIFICATION_ENDPOINT = "/notification";
	
	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
		// TODO Auto-generated method stub
		webSocketHandlerRegistry.addHandler(getNotificationWebSocketHandler(),NOTIFICATION_ENDPOINT)
		.setAllowedOrigins("*");
		
	}

	
	@Bean
	public WebSocketHandler getNotificationWebSocketHandler(){
		
		return new NotificationWebSocketHandler();
	}
	
	
}
