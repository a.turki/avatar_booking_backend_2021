package org.ab.repository;




import java.util.List;

import org.ab.models.Notification;
import org.ab.models.Rating;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
@Repository
@Transactional
public interface RatingRepository extends MongoRepository<Rating, String> {
	
	//Boolean existsByUsername(String username);
	Boolean existsByMissionId(String missionId);
	// Optional<User>findByEmail(String Email);
	//List<Rating> findByA
	@Query("{avatarEmail: ?0}")
	List<Rating> getRates(String avatarEmail);
}
