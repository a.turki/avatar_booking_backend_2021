package org.ab.repository;

import java.util.List;

import org.ab.models.Offer;
import org.ab.models.Reclamation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ReclamationRepository extends MongoRepository<Reclamation,String> {

	
	@Query("{missionID: ?0,claimerName: ?1}")
	Reclamation findByMissionandClaimer(String missionID,String claimerName);
	@Query("{missionID: ?0}")
	List<Reclamation> findByMission(String missionID);
	@Query("{missionID: ?0}")
	Reclamation findByMissionId(String missionID);
	@Query("{claimerName: ?0}")
	List<Reclamation> findByclaimerName(String claimerName);
	
}
