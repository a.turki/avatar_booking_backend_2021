package org.ab.repository;

import java.util.Optional;

import org.ab.models.ERole;
import org.ab.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role, String> {
	  Optional<Role> findByName(ERole name);
}
