package org.ab.repository;

import org.ab.models.Country;
import org.ab.models.Region;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CountryRepository extends MongoRepository<Country, String>{

	Boolean existsByName(String username);
}
