package org.ab.repository;

import java.util.Optional;

import org.ab.models.ERole;
import org.ab.models.Role;
import org.ab.payload.response.JwtResponse;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface JwtRepository extends MongoRepository<JwtResponse, String> {
	  
Optional<JwtResponse> findByToken(String token);

}
