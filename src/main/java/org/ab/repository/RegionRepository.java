package org.ab.repository;

import java.util.List;

import org.ab.models.Notification;
import org.ab.models.Rating;
import org.ab.models.Region;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface RegionRepository extends MongoRepository<Region, String> {
	
	Boolean existsByName(String username);
	@Query(value = "{country: ?0}")
	List<Region> getRegions(String country);
}
