package org.ab.repository;

import java.util.Optional;

import org.ab.models.Photo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
@Repository
@Transactional
public interface PhotoRepository extends MongoRepository<Photo, String> {
	
	@Query(value = "{theuser: ?0}")
	 Optional<Photo> findByTheuser(String name);
	 Boolean existsByTheuser(String name);
	 
	 
	 

}
