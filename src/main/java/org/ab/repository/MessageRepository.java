package org.ab.repository;

import java.util.List;

import org.ab.models.Message;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface MessageRepository extends MongoRepository<Message,String>{

	@Query("{$or:[{$and :[{senderId: ?0},{receiverId: ?1}]},{$and :[{receiverId: ?0},{senderId: ?1}]}]}")
	List<Message> getDiscussionsU1U2(String user1 , String user2);
	
	@Query(value="{$or:[{$and :[{senderId: ?0},{receiverId: ?1},{seen: ?2}]},{$and :[{receiverId: ?0},{senderId: ?1},{seen: ?2}]}]}", count = true)
	int unseenMessagecount(String user1 , String user2 , boolean seen);

	@Query("{$or:[{senderId: ?0},{receiverId: ?0}]}")
	List<Message> getDiscussionsUser(String user);
}
