package org.ab.repository;

import java.util.List;
import java.util.Optional;

import org.ab.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
@Repository
@Transactional
public interface UserRepository extends MongoRepository<User, String> {
		Optional<User> findById(String id);
	  Optional<User> findByUsername(String username);
	  @Query("{email: ?0}")
	  Optional<User>findByEmail(String Email);
      Boolean existsByUsername(String username);
	  Boolean existsByEmail(String email);
		
	  @Query("{city: ?0,isavatar: ?1}")
	  List<User> allAvatarByRegion(String region,boolean isavatar);
	  
	  
	  

}
