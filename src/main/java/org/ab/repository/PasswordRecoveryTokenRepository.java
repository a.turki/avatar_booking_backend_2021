package org.ab.repository;

import java.util.Optional;

import org.ab.models.PasswordRecoveryToken;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface PasswordRecoveryTokenRepository extends MongoRepository<PasswordRecoveryToken, String> {
	
	@Query("{usermail: ?0}")
	PasswordRecoveryToken findByusermail(String usermail);
	
	Boolean existsByUsermail(String usermail);
	
}
