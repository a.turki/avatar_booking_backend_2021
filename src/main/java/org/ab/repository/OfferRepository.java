package org.ab.repository;

import java.util.List;

import org.ab.models.Message;
import org.ab.models.Offer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface OfferRepository extends MongoRepository<Offer,String> {

	@Query("{missionId: ?0}")
	List<Offer> getOffersByMissionId(String missionId);
	@Query("{missionId: ?0,avatarMail: ?1}")
	Offer findOfferByMissionIDandAvatarMail(String missionId,String avatarMail);
}
