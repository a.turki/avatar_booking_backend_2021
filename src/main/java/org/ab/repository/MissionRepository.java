package org.ab.repository;

import java.util.List;

import org.ab.models.Mission;
import org.ab.models.Notification;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface MissionRepository extends MongoRepository<Mission,String>{

	
	@Query("{avataremail: ?0,rated: ?1}")
	List<Mission> getAvatarMissions(String avatarMail,Boolean rated);
	
	@Query("{clientemail: ?0}")
	List<Mission> getClientMissions(String clientemail);
	
	@Query(value = "{progress: ?0}",count = true)
	Number countProgress(String progress);
	
	@Query(value="{country: ?0}",count = true)
	Number countCountry(String country);

	
	
}
