package org.ab.repository;

import java.util.List;

import org.ab.models.Notification;
import org.ab.models.Offer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
@Repository
@Transactional
public interface NotificationRepository extends MongoRepository<Notification,String> {
	
	@Query(value = "{recipientUsername: ?0}", sort = "{sentAt: -1}")
	List<Notification> getMyNotifications(String recipientUsername);
	@Query("{recipientUsername: ?0,seen: ?1}")
	List<Notification> getMyUnseenNotifications(String recipientUsername,Boolean seen);
}
