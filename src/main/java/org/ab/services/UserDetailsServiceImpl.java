package org.ab.services;

import org.ab.models.User;
import org.ab.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
	@Autowired
	UserRepository userRepository;
	MongoTemplate mongoTemplate;
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

		return UserDetailsImpl.build(user);
	}
	
	 public String enableAppUser(String email) {
		 
	    	Query query1=new Query();
	    	query1.addCriteria(Criteria.where("email").is(email));
	    	
	    	User ct=mongoTemplate.findOne(query1, User.class);
	    	ct.setEnabled(true);
	    	mongoTemplate.save(ct);
	        return "user enabled";
	    }
}
