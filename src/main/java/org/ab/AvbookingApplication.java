package org.ab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvbookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvbookingApplication.class, args);
	}

}
